package a01b.e1;

import java.util.Optional;

import a01b.e1.ExamResult.Kind;

public class ExamResultFactoryImpl implements ExamResultFactory {
	
	private class ExamResultImpl implements ExamResult{
		
		private Kind kind;
		private boolean cumLaude;
		
		public ExamResultImpl(Kind kind, boolean cumLaude) {
			this.kind = kind;
			this.cumLaude = cumLaude;
		}

		@Override
		public Kind getKind() {
			return this.kind;
		}

		@Override
		public Optional<Integer> getEvaluation() {
			return Optional.empty();
		}

		@Override
		public boolean cumLaude() {
			return this.cumLaude;
		}

		@Override
		public String toString() {
			return kind.toString();
		}
		
	}
	
	private class ExamResultSucceded extends ExamResultImpl {
		
		private int evaluation;

		public ExamResultSucceded(Kind kind, int evaluation, boolean cumLaude) {
			super(kind, cumLaude);
			this.evaluation = evaluation;
		}
		
		@Override
		public Optional<Integer> getEvaluation() {
			return Optional.of(evaluation);
		}

		@Override
		public String toString() {
			if (cumLaude()) {
				return "SUCCEEDED(30L)";
			}
			else {
				return "SUCCEEDED("+this.evaluation+")";
			}
		}
		
	}
	
	@Override
	public ExamResult failed() {
		return new ExamResultImpl(Kind.FAILED, false);
	}

	@Override
	public ExamResult retired() {
		return new ExamResultImpl(Kind.RETIRED, false);
	}

	@Override
	public ExamResult succeededCumLaude() {
		return new ExamResultSucceded(Kind.SUCCEEDED, 30, true);
	}

	@Override
	public ExamResult succeeded(int evaluation) {
		if (evaluation <18 || evaluation>30) {
			throw new IllegalArgumentException();
		}
		return new ExamResultSucceded(Kind.SUCCEEDED, evaluation, false);
	}

}
