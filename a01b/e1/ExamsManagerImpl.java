package a01b.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import a01b.sol1.ExamResult;

public class ExamsManagerImpl implements ExamsManager {
	
	private Map<String, Map<String, ExamResult>> map = new HashMap<>();

	@Override
	public void createNewCall(String call) {
		map.putIfAbsent(call, new HashMap<>());

	}

	@Override
	public Set<String> getAllStudentsFromCall(String call) {					
		return this.map.get(call).keySet();
	}

	@Override
	public Map<String, Integer> getEvaluationsMapFromCall(String call) {
		return this.map.get(call).entrySet()
				.stream()
				.filter(p->p.getValue().getEvaluation().isPresent())
				.collect(Collectors.toMap(Entry::getKey, p->p.getValue().getEvaluation().get()));
}

	@Override
	public Map<String, String> getResultsMapFromStudent(String student) {
		return this.map.entrySet().stream()
				.filter(p -> p.getValue().containsKey(student))
				.collect(Collectors.toMap(Entry::getKey, p -> p.getValue().get(student).toString()));
	}

	@Override
	public Optional<Integer> getBestResultFromStudent(String student) {
		return this.map.values().stream() // a stream of maps student->result
				   .map(Map::entrySet)    // a stream of sets of entries student->result
				   .flatMap(Set::stream)  // a flattened stream of entries
			       .filter(e -> e.getKey().equals(student)) // just consider my student
			       .map(Entry::getValue) // stream of results
			       .filter(res -> res.getEvaluation().isPresent()) // only those with evaluation
			       .map(ExamResult::getEvaluation) // only evaluations (boxed in optional)
			       .map(Optional::get) // only evaluations (as int)
			       .max((x,y)->x-y); // max
	}

	@Override
	public void addStudentResult(String call, String student, a01b.e1.ExamResult result) {
		// TODO Auto-generated method stub
		
	}

}
