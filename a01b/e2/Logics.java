package a01b.e2;

public interface Logics {
	
	void setAlfiere(int x, int y);
	
	boolean isAlfiere(int x, int y);
	
	boolean isDiagonal(int x, int y);
	
	boolean hit (int x, int y);
	

}
