package a01b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2275891663575748017L;
	private static final int SIZE = 5;
    private final Map<JButton, Pair<Integer, Integer>> buttons = new HashMap<>();
    private Logics logic;
    
	public GUI() {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(SIZE*100, SIZE*100);
        this.logic = new LogicsImpl();
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            if(logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY())) {
            	System.exit(0);
            }
            if (logic.isDiagonal(buttons.get(bt).getX(), buttons.get(bt).getY())) {
            	logic.setAlfiere(buttons.get(bt).getX(), buttons.get(bt).getY());
            	this.draw();
            }
            if (logic.isAlfiere(buttons.get(bt).getX(), buttons.get(bt).getY())) {
            	this.enabled();
            }
            
        };
                
        for (int i=0; i<SIZE; i++){
        	for (int j = 0; j<SIZE; j++) {
	            final JButton jb = new JButton(" ");
	            jb.addActionListener(al);
	            if (i==0 && j==0) {
	            	jb.setText(" B ");
	            }
	            this.buttons.put(jb, new Pair<>(i, j));
	            panel.add(jb);
        	}
        }
        this.setVisible(true);
    }

	private void draw() {
		for (Map.Entry<JButton, Pair<Integer, Integer>> entry : buttons.entrySet()) {
			entry.getKey().setText(" ");
			if (logic.isAlfiere(entry.getValue().getX(), entry.getValue().getY())) {
				entry.getKey().setText(" B ");;
			}
		}
	}

	private void enabled() {
		for (Map.Entry<JButton, Pair<Integer, Integer>> entry : buttons.entrySet()) {
			if (logic.isDiagonal(entry.getValue().getX(), entry.getValue().getY()) || logic.isAlfiere(entry.getValue().getX(), entry.getValue().getY())) {
				entry.getKey().setEnabled(true);
			}
			else {
				entry.getKey().setEnabled(false);
			}
		}
	}
  
	
}
