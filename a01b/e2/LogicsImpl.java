package a01b.e2;

public class LogicsImpl implements Logics {
	
	private Pair<Integer, Integer> alfiere;
	private boolean selected;

	public LogicsImpl() {
		this.alfiere=new Pair<>(0,0);
		this.selected=false;
	}
	
	public void setAlfiere(int x, int y) {
		this.alfiere = new Pair<>(x, y);
	}

	@Override
	public boolean isAlfiere(int x, int y) {
		if (this.alfiere.equals(new Pair<>(x, y))) {
			this.selected = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean isDiagonal(int x, int y) {
		int diffX = this.alfiere.getX()-x;
		int diffY = this.alfiere.getY()-y;
		System.out.println(diffX);
		System.out.println(diffY);
		if (diffX!=0 && diffY!=0 && Math.abs(diffY)==Math.abs(diffX) && this.selected==true){
			return true;
		}
		return false;
	}

	@Override
	public boolean hit(int x, int y) {
		if (this.selected==true && this.isDiagonal(x,y) && x==0 && y==0) {
			return true;
		}
		return false;
	}
	

}
