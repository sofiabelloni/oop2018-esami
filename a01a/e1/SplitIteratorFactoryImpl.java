package a01a.e1;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class SplitIteratorFactoryImpl implements SplitIteratorFactory {

	@Override
	public SplitIterator<Integer> fromRange(int start, int stop) {
		return new SplitIterator<Integer>(){
			
			private int current = start;
			@Override
			public Optional<Integer> next() {
				return Optional.of(this.current++).filter(p -> p<=stop);
			}

			@Override
			public SplitIterator<Integer> split() {
				int oldCurrent = this.current;
				this.current=(this.current + stop)/2;
				return fromRange(oldCurrent, this.current-1);
			}
			
		};
	}

	@Override
	public SplitIterator<Integer> fromRangeNoSplit(int start, int stop) {
		return new SplitIterator<Integer>() {
			
			private int current = start;

			@Override
			public Optional<Integer> next() {
				return Optional.of(this.current++).filter(p -> p<=stop);
			}

			@Override
			public SplitIterator<Integer> split() {
				throw new UnsupportedOperationException();
			}
			
		};
	}

	@Override
	public <X> SplitIterator<X> fromList(List<X> list) {
		return map(fromRange(0, list.size()-1), list::get);
	}

	@Override
	public <X> SplitIterator<X> fromListNoSplit(List<X> list) {
		return map(fromRangeNoSplit(0, list.size()-1), list::get);
	}

	@Override
	public <X> SplitIterator<X> excludeFirst(SplitIterator<X> si) {
		si.next();
		return new SplitIterator<X>() {

			@Override
			public Optional<X> next() {
				return si.next();
			}

			@Override
			public SplitIterator<X> split() {
				return si.split();
			}
			
		};
	}

	@Override
	public <X, Y> SplitIterator<Y> map(SplitIterator<X> si, Function<X, Y> mapper) {
		// TODO Auto-generated method stub
		return null;
	}

}
