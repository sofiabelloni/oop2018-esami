package a01a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import a01a.sol2.Logics;
import a01a.sol2.LogicsImpl;
import a01a.sol2.Pair;

public class GUI extends JFrame {
    
	private static final long serialVersionUID = -6218820567019985015L;
    private final Map<JButton,Pair<Integer,Integer>> buttons = new HashMap<>();
    private final static int SIZE = 5;
    private final Logic logic;
    
    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100*SIZE, 100*SIZE);
        this.logic = new LogicImpl(SIZE);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            final Pair<Integer, Integer> pos = buttons.get(bt);
            if (logic.hit(pos.getX(),pos.getY())) {
            	System.exit(0);
            } else {
            	this.draw();
            }
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(i,j));
                panel.add(jb);
            }
        }
        this.draw();
        this.setVisible(true);
      }
    
    private void draw() {
    	for (final Map.Entry<JButton, Pair<Integer, Integer>> entry : buttons.entrySet()) {
    		entry.getKey().setText(" ");
    		if (logic.hasHorse(entry.getValue().getX(), entry.getValue().getY())) {
    			entry.getKey().setText(" K ");
    		}
    		if (logic.hasPedone(entry.getValue().getX(), entry.getValue().getY())) {
    			entry.getKey().setText(" * ");
    		}
    	}
    	
    }
  
}
