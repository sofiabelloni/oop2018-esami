package a01a.e2;

public interface Logic {
	
	boolean hit(int x, int y);
	
	boolean hasHorse(int x, int y);
	
	boolean hasPedone(int x, int y);

}
