package a01a.e2;

import java.util.Random;

public class LogicImpl implements Logic {
	
	private Pair<Integer, Integer> horse;
	private final Pair<Integer, Integer> pedone;
	private final Random random = new Random();
	private final int size;
	
	public LogicImpl(int size) {
		this.size = size;
		this.pedone = this.randomPosition();
		this.horse = this.randomPosition();
	}
	
	private Pair<Integer, Integer> randomPosition(){
		Pair<Integer, Integer> pos = new Pair<>(this.random.nextInt(size), this.random.nextInt(size));
		return pos;
	}

	@Override
	public boolean hit(int x, int y) {
		//check if is permitted
		Pair<Integer, Integer> pos = new Pair<>(x, y);
		int diffX = this.horse.getX()-x;
		int diffY = this.horse.getY()-y;
		if( diffX!=0 && diffY!=0 && Math.abs(diffX)+Math.abs(diffY)==3) {
			this.horse = pos;
			return (this.horse.equals(this.pedone));
		}
		return false ;
	}

	@Override
	public boolean hasHorse(int x, int y) {
		Pair<Integer, Integer> pos = new Pair<>(x, y);
		return this.horse.equals(pos);
	}

	@Override
	public boolean hasPedone(int x, int y) {
		Pair<Integer, Integer> pos = new Pair<>(x, y);
		return this.pedone.equals(pos);
	}

}
